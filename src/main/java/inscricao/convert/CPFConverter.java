/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.convert;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author andre
 */
@FacesConverter("cpfConverter")
public class CPFConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        
        if (!Pattern.matches("^\\d{11}|\\d{9}-\\d{2}|\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$", string)) {
            FacesMessage msg =
                new FacesMessage("CPF 'valor digitado' inválido");
            throw new ConverterException(msg);
        }
        
        return string == null || string.trim().isEmpty() ? null : Long.valueOf((string.replace("-", "")).replace(".", ""));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String cpf = o.toString();
        return String.format("%s.%s.%s-%s", cpf.substring(0, 3),cpf.substring(3, 6),cpf.substring(6, 9),cpf.substring(9));
    }

    
}
